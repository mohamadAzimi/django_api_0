from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from .serializers import *

class HelloApi(APIView):
    """ Test our api """
    serializer_class = HelloSerializer

    def get(self, request):
        an_apiview = [
            'you have more control over api logic',
            'provide (post, delete, get, put, patch)'
        ]
        return Response(
            {"msg": "hello world", "api_view": an_apiview}
        )


    def post(self, request):
        """ create hello massage with our name """
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid(raise_exception=True):
            name = serializer.validated_data.get('name')
            return Response(
                {'massage': f"hello {name}"}
            )
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


    def put(self, request, pk: None):
        """ handel updating an obj """
        return Response({'msg': 'PUT method'})


    def patch(self, request, pk: None):
        """ handel a partial updating  of an obj """
        return Response({'msg': 'PATCH method'})


    def delete(self, request, pk: None):
        """ delete an obj """
        return Response({"msg": "DELETE"})

class HelloViewSet(viewsets.ViewSet):
    """ Test Api ViewSet"""
    serializer_class = HelloSerializer

    def list(self, request):
        """ retrieve oll obj """
        sample = [
            'hello world',
            'hey sandwich man'
        ]
        return Response({'1': sample})


    def create(self, request):
        """ create new hello massage """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            return Response(
                {'msg': 'hello js react django developer'},
                status=status.HTTP_201_CREATED
            )

    def retrieve(self, request, pk=None):
        """ handle getting an obj """
        return Response({'msg': 'GET'})


    def update(self, request, pk=None):
        """ handle updating an obj """
        return Response({"msg": "UPDATE"})


    def partial_update(self, request, pk=None):
        """ handle partial of an obj """
        return Response({"msg": "partial update"})


    def destroy(self, request, pk=None):
        """ handle deleting an obj """
        return Response({"msg": "delete_http"})


    # TODO 09 -> 006