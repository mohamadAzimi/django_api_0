from rest_framework import serializers
from .models import UserProfile

class HelloSerializer(serializers.Serializer):
    """ serialize a name fir testing APIView """
    name = serializers.CharField(max_length=10)